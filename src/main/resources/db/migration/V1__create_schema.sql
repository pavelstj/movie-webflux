CREATE TABLE movie
(
  id          BIGINT PRIMARY KEY,
  title       TEXT,
  overview    TEXT,
  poster_path TEXT
);

CREATE TABLE person
(
  id   BIGSERIAL PRIMARY KEY,
  name TEXT NOT NULL
);