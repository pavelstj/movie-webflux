package com.epolsoft.simpleproj4;

import com.epolsoft.simpleproj4.config.CustomApplicataionProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(CustomApplicataionProperties.class)
public class Simpleproj4Application {

    public static void main(String[] args) {
        SpringApplication.run(Simpleproj4Application.class, args);
    }

}
