package com.epolsoft.simpleproj4.repository;


import com.epolsoft.simpleproj4.model.Movie;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

//TODO REFACTOR
@Repository
public class MovieRepository {

    private static List<Movie> movieFlux = new ArrayList<>();

    public Flux<Movie> getMovieFlux() {
        return Flux.fromIterable(movieFlux);
    }

    public void setMovieFlux(Movie movie) {
        movieFlux.add(movie);
    }

}
