package com.epolsoft.simpleproj4.repository;

import com.epolsoft.simpleproj4.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.r2dbc.function.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

//TODO REFACTOR
@Repository
public class MovieDBRepository{

    public final DatabaseClient databaseClient;

    @Autowired
    public MovieDBRepository(DatabaseClient databaseClient) {
        this.databaseClient = databaseClient;
    }

    public Mono<Void> deleteMovieById(long id) {
        return databaseClient.execute()
                .sql("delete from movs where id = $1")
                .bind("$1", id)
                .then();
    }

    public Mono<Movie> findMovieById(String id) {
        return databaseClient.execute()
                .sql("select id, title, overview, poster_path from movs where id = $1")
                .bind("$1", id)
                .as(Movie.class)
                .fetch().first().doOnSuccess(System.out::println);
    }

    public Flux<Movie> findAllMovies() {
        return databaseClient.execute()
                .sql("select * from movs")
                .as(Movie.class)
                .fetch().all().doOnComplete(System.out::println);
    }

    public Mono<Void> addMovie(Movie movie) {
        return databaseClient.execute()


//                )
//                .value("id", movie.getId())
//                .execute()
                .sql("insert into movs(id, title, overview, poster_path) values (123, 'jgjhg', 'jgjh569g', 'jgjhg12')")

//                .bind("$1", movie.getId())
//                .bind("$2", movie.getTitle())
//                .bind("$3", movie.getOverview())
//                .bind("$4", movie.getPoster_path())
                .then();
    }

    public Mono<Void> createTable() {
        return databaseClient.execute()
                .sql("create table person (id varchar(30), name varchar(255))")
                .then();
    }

}
