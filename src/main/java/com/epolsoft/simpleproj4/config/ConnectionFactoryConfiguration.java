package com.epolsoft.simpleproj4.config;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConnectionFactoryConfiguration {

    @Value("${application.database.host}")
    private String host;

    @Value("${application.database.port}")
    private int port;

    @Value("${application.database.name}")
    private String databaseName;

    @Value("${application.database.user}")
    private String userName;

    @Value("${application.database.password}")
    private String password;

    @Bean
    public ConnectionFactory connectionFactory() {
        return new PostgresqlConnectionFactory(
                PostgresqlConnectionConfiguration.builder()
                        .host(host)
                        .port(port)
                        .database(databaseName)
                        .username(userName)
                        .password(password)
                        .build()
        );
    }

}
