package com.epolsoft.simpleproj4.config;

import com.epolsoft.simpleproj4.person.Person;
import com.epolsoft.simpleproj4.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Configuration
@EnableWebFlux
@ComponentScan("com.epolsoft.simpleproj4")
public class PersonRouter implements WebFluxConfigurer {

    @Autowired
    private PersonRepository personRepository;

    @Bean
    RouterFunction<ServerResponse> composedRoutes() {
        return RouterFunctions
                .route(GET("/person"),
                        serverRequest -> ok()
                                .body(personRepository.findAll(), Person.class))
                .andRoute(GET("/person/{id}"),
                        serverRequest -> ok()
                                .body(personRepository.findById(Long.parseLong(serverRequest.pathVariable("id"))), Person.class))

                .andRoute(POST("/person"),
                        serverRequest -> serverRequest.bodyToMono(Person.class)
                                .doOnNext(person -> personRepository.save(person))
                                .then(ok().build()));
//                .andRoute(GET("/movie"),
//                        serverRequest -> ok().body(personWebClient.getMovie() , Void.class))
//                .andRoute(GET("/movie/{id}"),
//                        serverRequest -> ok().body(movieDBRepository
//                                .findMovieById(serverRequest.pathVariable("id")), Movie.class));
    }

}
