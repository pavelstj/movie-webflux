package com.epolsoft.simpleproj4.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "application")
@Data
public class CustomApplicataionProperties {

    @Data
    public static class Database {

        private String host;

        private int port;

        private String name;

        private String user;

        private String password;
    }
}
