package com.epolsoft.simpleproj4.person;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PersonRepositoryTest {

    @Autowired
    PersonRepository personRepository;

    @Test
    public void save() {
        Flux<Person> personFlux = Flux.just("vasyan1", "vasyan2", "vasyan3", "vasyan4", "vasyan5")
                .map(name -> new Person().setName(name))
                .flatMap(person -> personRepository.save(person));

        StepVerifier.create(personFlux)
                .expectNextCount(5)
                .verifyComplete();
    }
}